---
title: "Mount Disk Permanently on Linux"
date: 2019-04-08T15:38:58+02:00
tags: ["linux", "amazon", "ebs"]
draft: false
---

In case you want to permanently mount an (Amazon EBS for instance) volume on a machine.

# Prepare

List partitions and read out unmounted volume:
```
sudo lsblk
```

In this case, `sdb` is the volume we want to mount.
Format the volume with a proper filesystem, `ext4`:
```
sudo mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/sdb
```

Create the directory you want to mount to and mount:
```
sudo mkdir -p /mnt/disks/data
sudo mount -o discard,defaults /dev/sdb /mnt/disks/data
```

Optionally set correct permissions:
```
sudo chmod a+w /mnt/disks/data
```

At this point, your mounted volume is ready to use.

# Permament

In order to have the volume mounted each time we restart the system, setup automount in `fstab`.

First make a backup of the existing `fstab` file:
```
sudo cp /etc/fstab /etc/fstab.backup
```

Read out the UUID of the device.
```
sudo blkid /dev/sdb
```

Open the `fstab` file:
```
sudo vim /etc/fstab
```

Add the following line to the file. Replace `#ID#` with the UUID of the device.
```
UUID=#ID# /mnt/disks/data ext4 discard,defaults,nofail 0 2
```
