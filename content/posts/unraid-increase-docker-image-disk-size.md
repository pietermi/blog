---
title: "Unraid Increase Docker Image Disk Size"
date: 2020-07-14T15:10:54+02:00
tags: ["linux", "unraid", "docker"]
draft: false
---

Instructions for how to increase the Docker Image disk size in Unraid.

In Unraid:

```
1. Stop Docker service in Settings > Docker.
2. Increase disk size and start service again.
```

In most cases this is the wrong approach and you should be looking into the reason why your Docker image size is increasing. Most of the time this is due to misconfigured Docker applications. Make sure you set all Docker paths to location outside of the Docker container itself.
