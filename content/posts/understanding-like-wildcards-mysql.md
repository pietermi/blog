---
title: "Understanding LIKE Wildcards in MySQL"
date: "2024-02-19"
description: "Learn how to use _ and % wildcards with the LIKE operator in MySQL."
tags: ["mysql", "sql", "wildcards", "database"]
---

# Understanding LIKE Wildcards in MySQL

In MySQL, the `LIKE` operator supports two wildcard characters for pattern matching. The underscore `_` represents a **single character**, while the percent sign `%` matches **multiple characters**.  

Example usage:  

```sql
SELECT * FROM users WHERE name LIKE 'J_n%';
```

This query matches names like "Jon", "Jen", or "Janet" but **not** "Jeans". Understanding these wildcards can help refine search queries efficiently.
