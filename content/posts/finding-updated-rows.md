---
title: "Using SELECT Inside INSERT in PostgreSQL"
date: "2025-01-24"
description: "Learn how to insert data dynamically using a SELECT statement in PostgreSQL."
tags: ["sql", "postgresql", "database"]
---

# Using SELECT Inside INSERT in PostgreSQL

In SQL, you can use a `SELECT` statement inside an `INSERT` to insert data dynamically from another table. For example, the following query inserts records into `books` by selecting values from the `authors` table:  

```sql
INSERT INTO books (author_id, distributor_id, is_archived, reference) 
SELECT id, 2, TRUE, token FROM authors WHERE token = '';
```

This approach is useful when you need to copy or transform data while inserting, avoiding manual entry. It helps streamline database operations and ensures consistency across related tables.
