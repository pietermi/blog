---
title: "Handling Unique Constraints in Ecto with on_conflict"
date: "2024-06-07"
description: "Use on_conflict and conflict_target in Ecto to manage unique constraints in PostgreSQL."
tags: ["elixir", "ecto", "postgresql", "database"]
---

# Handling Unique Constraints in Ecto with on_conflict

When inserting data in Ecto, you may encounter unique constraint violations that clutter your logs. Instead of letting these errors flood your system, you can use `on_conflict` and `conflict_target` to define how PostgreSQL should handle conflicts.  

Example usage:  

```elixir
Repo.insert!(
  %User{email: "test@example.com"},
  on_conflict: {:replace, [:name]},
  conflict_target: [:email]
)
```

This instructs PostgreSQL to update the name field if a conflict occurs on the email column. Properly handling conflicts improves database efficiency and keeps logs clean.
