---
title: "Print Lines Above and Below a grep Match"
date: "2024-09-30"
description: "Use grep with -C to display surrounding lines for better context."
tags: ["linux", "grep", "cli"]
---

# Print Lines Above and Below a grep Match

When searching for text in a file using `grep`, you may want to see the surrounding lines for context. You can use the `-C` option followed by a number to display lines before and after the match. For example, the following command shows two lines above and below the found entry:

```sh
grep -C 2 "search_term" filename.txt
```

This is particularly useful for log analysis, debugging, or reviewing configuration files.
